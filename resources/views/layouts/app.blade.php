<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <!--link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" -->   
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/iriy-admin.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('demo/css/demo.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}">

        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('css/plugins/rickshaw.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/plugins/morris.min.css') }}">

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>

    <body class="">                
        
        <div id="app" style="margin-top: 100px;">        
            @yield('content')
        </div>

        <!-- Scripts -->
        <!-- script src="{{ URL::asset('js/app.js') }}"></script -->     

        <script src="{{ URL::asset('assets/libs/jquery/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/bs3/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-navgoco/jquery.navgoco.js') }}"></script>
        <script src="{{ URL::asset('js/main.js') }}"></script>
    </body>
</html>
