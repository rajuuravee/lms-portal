<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <!-- link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" -->  
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/iriy-admin.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('demo/css/demo.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}">

        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css') }}"/>
        <link rel="stylesheet" href="{{ URL::asset('css/plugins/rickshaw.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/plugins/morris.min.css') }}">

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>

    <body class="">
        
        <header>
            <nav class="navbar navbar-default navbar-static-top no-margin" role="navigation">
                <div class="navbar-brand-group">
                    <a class="navbar-sidebar-toggle navbar-link" data-sidebar-toggle>
                        <i class="fa fa-lg fa-fw fa-bars"></i>
                    </a>
                    <a class="navbar-brand hidden-xxs" href="index.html">
                        <span class="sc-visible">
                            I
                        </span>
                        <span class="sc-hidden">
                            <span class="semi-bold">                                
                                {{ config('app.name', 'Laravel') }}                                
                            </span>                            
                        </span>
                    </a>
                </div>                
                <ul class="nav navbar-nav navbar-nav-expanded pull-right margin-md-right">
                    <li class="hidden-xs">
                        <form class="navbar-form">
                            <div class="navbar-search">
                                <input type="text" placeholder="Search &hellip;" class="form-control">
                                <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                            <i class="glyphicon glyphicon-envelope"></i>
                            <span class="badge badge-up badge-dark badge-small">3</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages pull-right">
                            <li class="dropdown-title bg-inverse">New Messages</li>
                            <li class="unread">
                                <a href="javascript:;" class="message">
                                    <img class="message-image img-circle" src="{{ URL::asset('demo/images/avatars/1.jpg') }}">

                                    <div class="message-body">
                                        <strong>Ernest Kerry</strong><br>
                                        Hello, You there?<br>
                                        <small class="text-muted">8 minutes ago</small>
                                    </div>
                                </a>
                            </li>
                            <li class="unread">
                                <a href="javascript:;" class="message">
                                    <img class="message-image img-circle" src="{{ URL::asset('demo/images/avatars/3.jpg') }}">

                                    <div class="message-body">
                                        <strong>Don Mark</strong><br>
                                        I really appreciate your &hellip;<br>
                                        <small class="text-muted">21 hours</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="message">
                                    <img class="message-image img-circle" src="{{ URL::asset('demo/images/avatars/8.jpg') }}">

                                    <div class="message-body">
                                        <strong>Jess Ronny</strong><br>
                                        Let me know when you free<br>
                                        <small class="text-muted">5 days ago</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="message">
                                    <img class="message-image img-circle" src="{{ URL::asset('demo/images/avatars/7.jpg') }}">

                                    <div class="message-body">
                                        <strong>Wilton Zeph</strong><br>
                                        If there is anything else &hellip;<br>
                                        <small class="text-muted">06/10/2014 5:31 pm</small>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-footer">
                                <a href="javascript:;"><i class="fa fa-share"></i> See all messages</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                            <i class="glyphicon glyphicon-globe"></i>
                            <span class="badge badge-up badge-danger badge-small">3</span>
                        </a>
                        <ul class="dropdown-menu dropdown-notifications pull-right">
                            <li class="dropdown-title bg-inverse">Notifications (3)</li>
                            <li>
                                <a href="javascript:;" class="notification">
                                    <div class="notification-thumb pull-left">
                                        <i class="fa fa-clock-o fa-2x text-info"></i>
                                    </div>
                                    <div class="notification-body">
                                        <strong>Call with John</strong><br>
                                        <small class="text-muted">8 minutes ago</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="notification">
                                    <div class="notification-thumb pull-left">
                                        <i class="fa fa-life-ring fa-2x text-warning"></i>
                                    </div>
                                    <div class="notification-body">
                                        <strong>New support ticket</strong><br>
                                        <small class="text-muted">21 hours ago</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="notification">
                                    <div class="notification-thumb pull-left">
                                        <i class="fa fa-exclamation fa-2x text-danger"></i>
                                    </div>
                                    <div class="notification-body">
                                        <strong>Running low on space</strong><br>
                                        <small class="text-muted">3 days ago</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="notification">
                                    <div class="notification-thumb pull-left">
                                        <i class="fa fa-user fa-2x text-muted"></i>
                                    </div>
                                    <div class="notification-body">
                                        New customer registered<br>
                                        <small class="text-muted">06/18/2014 12:31 am</small>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-footer">
                                <a href="javascript:;"><i class="fa fa-share"></i> See all notifications</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle navbar-user" href="javascript:;">
                            <img class="img-circle" src="{{ URL::asset('demo/images/profile.jpg') }}">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="arrow"></li>
                            <li><a href="javascript:;">Profile</a></li>
                            <li><a href="javascript:;"><span class="badge badge-danger pull-right">2</span> Inbox</a></li>
                            <li><a href="javascript:;">Messages</a></li>
                            <li><a href="javascript:;">Settings</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>                  
            </nav>
        </header>
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
                <div class="sidebar-profile">
                    <img class="img-circle profile-image" src="{{ URL::asset('demo/images/profile.jpg') }}">

                    <div class="profile-body">
                        <h4>{{ Auth::user()->name }}</h4>

                        <div class="sidebar-user-links">
                            <a class="btn btn-link btn-xs" href="javascript:;"          data-placement="bottom" data-toggle="tooltip" data-original-title="Profile"><i class="fa fa-user"></i></a>
                            <a class="btn btn-link btn-xs" href="javascript:;"          data-placement="bottom" data-toggle="tooltip" data-original-title="Messages"><i class="fa fa-envelope"></i></a>
                            <a class="btn btn-link btn-xs" href="javascript:;"          data-placement="bottom" data-toggle="tooltip" data-original-title="Settings"><i class="fa fa-cog"></i></a>
                            <a class="btn btn-link btn-xs" href="{{ url('/logout') }}"  data-placement="bottom" data-toggle="tooltip" data-original-title="Logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i></a>
                        </div>
                    </div>
                </div>
                <nav>
                    <h5 class="sidebar-header">Navigation</h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="nav-dropdown open"> 
                            <a href="#" title="Home">
                                <i class="fa fa-lg fa-fw fa-home"></i> Home
                            </a>
                            <ul class="nav-sub">
                                <li class="active open">
                                    <a href="{{ url('home') }}" title="home">
                                        <i class="fa fa-fw fa-caret-right"></i> Home
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-dropdown">
                            <a href="#" title="Users">
                                <i class="fa fa-lg fa-fw fa-user"></i> Users
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a href="#" title="Members">
                                        <i class="fa fa-fw fa-caret-right"></i> Members
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-dropdown">
                            <a href="#" title="Courses">
                                <i class="fa fa-lg fa-fw fa-edit"></i> Courses                                
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a href="#" title="New Courses">
                                        <i class="fa fa-fw fa-caret-right"></i> New Courses
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Manage Courses">
                                        <i class="fa fa-fw fa-caret-right"></i> Manage Courses
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-dropdown">
                            <a href="#" title="Assignments">
                                <i class="fa fa-lg fa-fw fa-file-text"></i> Assignments
                            </a>            
                            <ul class="nav-sub">
                                <li>
                                    <a href="#" title="In Class Assignments">
                                        <i class="fa fa-fw fa-caret-right"></i> In Class Assignments
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Take Home Assignments">
                                        <i class="fa fa-fw fa-caret-right"></i> Take Home Assignments
                                    </a>
                                </li>                                
                            </ul>                
                        </li>
                        <li class="nav-dropdown">
                            <a href="#" title="Tutorials">
                                <i class="fa fa-lg fa-fw fa-file-video-o"></i> Tutorials
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a href="#" title="Paper Tutorials">
                                        <i class="fa fa-fw fa-caret-right"></i> Paper Tutorials
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Video Tutorials">
                                        <i class="fa fa-fw fa-caret-right"></i> Video Tutorials
                                    </a>
                                </li>                                
                            </ul>
                        </li>                                                
                    </ul>                    
                    <h5 class="sidebar-header">Footer</h5>
                    <ul class="sidebar-summary">
                        <li>
                            <div class="mini-chart mini-chart-block">
                                <div class="chart-details">
                                    <div class="chart-name">
                                        <a href="#" title="Our Story">Our Story
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mini-chart mini-chart-block">
                                <div class="chart-details">
                                    <div class="chart-name">
                                        <a href="#" title="Our Team">Our Team
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mini-chart mini-chart-block">
                                <div class="chart-details">
                                    <div class="chart-name">
                                        <a href="#" title="Contact us">Contact us
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="mini-chart mini-chart-block">
                                <div class="chart-details">
                                    <div class="chart-name">
                                        <a href="#" title="FAQ">FAQ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
            </aside>
            
            <div class="page-content">
                <div class="page-subheading page-subheading-md">
                    <ol class="breadcrumb">
                        <li class="active"><a href="javascript:;">Dashboard</a></li>
                    </ol>
                </div>
                                        
                <div id="app">         
                    @yield('content')
                </div>
                
            </div>
            
        </div>    

        <!-- Scripts -->
        <!-- script src="{{ URL::asset('js/app.js') }}"></script -->     

        <script src="{{ URL::asset('assets/libs/jquery/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/bs3/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-navgoco/jquery.navgoco.js') }}"></script>
        <script src="{{ URL::asset('js/main.js') }}"></script>

        <!--[if lt IE 9]>
        <script src="{{ URL::asset('assets/plugins/flot/excanvas.min.js') }}"></script>
        <![endif]-->

        <script src="{{ URL::asset('assets/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>
        <script src="{{ URL::asset('demo/js/demo.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-jvectormap/maps/world_mill_en.js') }}"></script>
        <!--[if gte IE 9]>-->
        <script src="{{ URL::asset('assets/plugins/rickshaw/js/vendor/d3.v3.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/rickshaw/rickshaw.min.js') }}"></script>
        <!--<![endif]-->
        <script src="{{ URL::asset('assets/plugins/flot/jquery.flot.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/raphael/raphael-min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ URL::asset('demo/js/dashboard.js') }}"></script>
    </body>
</html>
