<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    //facebook
    'facebook'  => [
        'client_id'     => '248760665542330',
        'client_secret' => '7da7dcdaf9f6264d3ec85790c8ce2e7d',
        'redirect'      => 'http://localhost/lms-portal/public/index.php/callback/facebook',
    ],
    
    //linkedin
    'linkedin'  => [
        'client_id'     => '81z7uvmis568d0',
        'client_secret' => 'F1NQ0AUB7BtRsfVf',
        'redirect'      => 'http://localhost/lms-portal/public/index.php/callback/linkedin',
    ],
    
    //google
    'google'    => [
        'client_id'     => '179348460853-8pn7vm4haan2f59tisgcmsh8eahs0imv.apps.googleusercontent.com',
        'client_secret' => '5HWh872HVQdHCIjQgApQ_IlN',
        'redirect'      => 'http://localhost/lms-portal/public/index.php/callback/google',
    ],

];
