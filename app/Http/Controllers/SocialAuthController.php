<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    //Google
    public function loginGoogle()
    {
        return Socialite::driver('google')->redirect(); 
    }

    public function callbackGoogle(SocialAccountService $service) 
    {
        $provider = 'google';
        
        $user = $service->createOrGetUser(Socialite::driver($provider)->user(), $provider);

        auth()->login($user);

        return redirect()->to('/home');
        
    } 
    
    //facebook
    public function loginFacebook()
    {
        return Socialite::driver('facebook')->redirect(); 
    }

    public function callbackFacebook(SocialAccountService $service) 
    {
        $provider = 'facebook';
        
        $user = $service->createOrGetUser(Socialite::driver($provider)->user(), $provider);

        auth()->login($user);

        return redirect()->to('/home');
        
    } 
    
    
    //linkedin
    public function loginLinkedin()
    {
        return Socialite::driver('linkedin')->redirect(); 
    }

    public function callbackLinkedin(SocialAccountService $service, Request $request) 
    {
        
        $error = $request->input('error');
        
        if(!empty($error))        
        {
            return redirect()->to('/login');
        }
        
        $provider = 'linkedin';
        
        $user = $service->createOrGetUser(Socialite::driver($provider)->user(), $provider);

        auth()->login($user);

        return redirect()->to('/home');
        
    } 
    
}
