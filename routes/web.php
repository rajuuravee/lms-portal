<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});

//google login route
Route::get('/login/google', 'SocialAuthController@loginGoogle');
Route::get('/callback/google', 'SocialAuthController@callbackGoogle');  

//facebook login route
Route::get('/login/facebook', 'SocialAuthController@loginFacebook');
Route::get('/callback/facebook', 'SocialAuthController@callbackFacebook');

//linkedin login route
Route::get('/login/linkedin', 'SocialAuthController@loginLinkedin');
Route::get('/callback/linkedin', 'SocialAuthController@callbackLinkedin');   

//auth route
Auth::routes();

//access route after login
Route::get('/home', 'HomeController@index');
